const WebSocket = require("ws");
const http = require("http");
const fs = require("fs");
const url = require("url");
const badWords = require("bad-words");

let data = "";

const filter = new badWords();

const server = http.createServer((req, res) => {
  const parsedUrl = url.parse(req.url);

  switch (parsedUrl.pathname) {
    case "/element":
      fs.readFile("src/static/element.html", "utf8", (err, data) => {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(data);
      });
      break;

    case "/":
      fs.readFile("src/static/widget.html", "utf8", (err, data) => {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(data);
      });
      break;

    default:
      res.writeHead(404, { "Content-Type": "text/html" });
      res.end("Page not found");
  }
});

const wss = new WebSocket.Server({ server });

wss.broadcast = function broadcast(msg) {
  console.log("here are mess:", msg);
  wss.clients.forEach(function each(client) {
    client.send(msg);
  });
};
wss.on("connection", (ws) => {
  console.log("Client connected.");

  ws.on("message", (message) => {
    const parsedMessage = JSON.parse(message);
    const userNick = JSON.stringify(parsedMessage.user);

    if (filter.isProfane(userNick)) {
      const changedMessage = JSON.stringify({
        emoji: "*",
        user: "",
      });
      console.log(`Received message: ${changedMessage}`);
      return wss.broadcast(`${changedMessage}`);
    }
    console.log(`Received message: ${message}`);
    return wss.broadcast(`${message}`);
  });

  ws.addEventListener("close", (event) => {
    console.log("WebSocket closed:", event);
  });

  ws.addEventListener("error", (event) => {
    console.log("WebSocket error:", event);
  });

  ws.send("Hello from the server!");

  // setInterval(() => {
  //     ws.send('Real-time update from the server.');
  // }, 1000);
});

const port = 8080;
server.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
